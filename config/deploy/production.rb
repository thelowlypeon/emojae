# deploy using:
#
# cap production deploy
#
# this will deploy using deploy@emojis, with these ssh configs:
# Host do_containers
#   HostName 138.197.104.41
#   User root
# Host unicorn
#   User deploy
#   ProxyCommand ssh -q do_containers  nc -q0 10.0.3.79 22
#
# note: you may need to restart unicorn.
# `/home/deploy/bin/unicorn_server restart` doesn't always work.
# if so, as deploy@emojes, use:
#   /home/deploy/bin/unicorn_server stop
#   /home/deploy/bin/unicorn_server start

server 'emojes',
  user: 'deploy',
  roles: %w{app web}

namespace :deploy do
   task :restart do
     on roles(:app), in: :sequence, wait: 5 do
       puts `pwd`
       `/home/deploy/bin/unicorn_server restart`
     end
   end

   after :finishing, "deploy:cleanup"
 end
