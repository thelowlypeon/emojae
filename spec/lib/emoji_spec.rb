require File.expand_path '../../spec_helper.rb', __FILE__

describe Emogae::Emoji do
  let(:codes) { [''] }
  let(:emoji) { Emogae::Emoji.new(codes) }

  context "#file_exists?" do
    subject { emoji.file_exists? }

    context "for invalid codes" do
      it "doesn't return a path" do
        expect(subject).to be_falsey
      end
    end

    context "for bullshit emoji" do
      let(:codes) { ['01'] }
      it "doesn't return a path" do
        expect(subject).to be_falsey
      end
    end

    context "for valid emoji" do
      let(:codes) { ['1f346'] }
      it "should return a valid path" do
        expect(subject).to be_truthy
      end
    end

    context "for two character emoji" do
      let(:codes) { ['1f4aa', '1f3ff'] }
      it "should return a valid path" do
        expect(subject).to be_truthy
      end
    end
  end

  context "#humanize" do
    context "with single character emoji" do
      let(:codes) { ['1f346'] }
      it "should know it's a eggplant" do
        expect(emoji.name).to eq 'eggplant'
      end
    end

    context "with two character emoji" do
      let(:codes) { ['1f4aa', '1f3ff'] }
      it "should know it's a muscle of a specific skin tone" do
        expect(emoji.name).to eq 'muscle_tone5'
      end
    end
  end
end
