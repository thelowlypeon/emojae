require File.expand_path '../../spec_helper.rb', __FILE__

describe Emogae::ImageHelper do
  let(:input) { "😎💪🏿" }
  let(:eggplant) { Emogae::Emoji.new(['1f346']) }
  let(:muscle) { Emogae::Emoji.new(['1f4aa', '1f3ff']) }

  it "joins images" do
    local_path = "/tmp/#{Time.now}.png"
    emojis = [eggplant, muscle]
    Emogae::ImageHelper.new.join_emojis(emojis, local_path)
    expect(File.exists?(local_path)).to be_truthy
  end
end
