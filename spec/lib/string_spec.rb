require File.expand_path '../../spec_helper.rb', __FILE__

describe Emogae::String do
  let(:input) { "" }
  let(:string) { Emogae::String.new(input) }

  context "#from_param" do
    let(:input) { "1f4aa-1f3ff-1f4af" }
    let(:string) { Emogae::String.from_param(input) }

    it "constructs the string" do
      expect(string).to eq "💪🏿💯"
    end

    context "with invalid input" do
      let(:input) { '-F495' }

      it "doesn't shit itself" do
        expect(string).to_not be_nil
      end
    end
  end

  context "#from_human" do
    let(:input) { "eggplant, eggplant" }
    let(:string) { Emogae::String.from_human(input) }

    it "constructs the string" do
      expect(string).to eq "🍆🍆"
    end

    context "with two character emoji" do
      let(:input) { "muscle_tone5" }
      it "constructs the string" do
        expect(string).to eq "💪🏿"
      end
    end

    context "with bullshit values" do
      let(:input) { 'fart' } #thanks daisy
      it "doesn't shit itself" do
        expect(string).to eq ""
      end
    end
  end

  context "#to_param" do
    let(:input) { "🍆🍆" }
    subject { string.to_param }

    it "builds a url segment" do
      expect(subject).to eq '1f346-1f346'
    end

    it "builds a URI encoded segment" do
      expect(URI.encode(subject)).to eq subject
    end
  end

  context "#string_to_unicodes" do
    subject { string.unicodes }

    context "when using single character emoji" do
      let(:input) { "🍆🍆" }

      it "should find the correct codes" do
        eggplant = '1f346'
        expect(subject).to eq [eggplant, eggplant]
      end
    end

    context "when using double character emoji" do
      let(:input) { "💪🏿💪🏿" }

      it "should find the correct codes" do
        muscle = '1f4aa'
        skin = '1f3ff'
        expect(subject).to eq [muscle, skin, muscle, skin]
      end
    end
  end

  context "#unicodes_to_emojis" do
    let(:unicodes) { [] }
    before { string.instance_variable_set(:@unicodes, unicodes) }
    subject { string.emojis }

    context "when using bullshit emoji" do
      let(:unicodes) { ['01'] }
      it "shouldn't find emojis" do
        expect(subject.count).to eq 0
      end
    end

    context "when using single character emoji" do
      let(:unicodes) { ['1f346', '1f346'] }
      it "should return an emoji for each" do
        expect(subject.count).to eq 2
        subject.each do |emoji|
          expect(emoji.file_exists?).to be_truthy
        end
      end
    end

    context "when using double character emoji" do
      let(:unicodes) { ['1f4aa', '1f3ff'] }

      it "should group the codes into one emoji" do
        expect(subject.count).to eq 1
      end
    end

    context "when using mixed count emoji" do
      let(:unicodes) { ['1f346', '1f4aa', '1f3ff'] }

      it "should group the shared codes into one emoji" do
        expect(subject.count).to eq 2
      end
    end
  end
end
