require 'rack/test'

ENV['RACK_ENV'] = 'test'

require File.expand_path '../../bootstrap.rb', __FILE__

module RSpecMixin
  include Rack::Test::Methods
  def app() Emogae::Site end
end

RSpec.configure do |c|
  c.include RSpecMixin

  c.filter_run focus: true
  c.run_all_when_everything_filtered = true
end
