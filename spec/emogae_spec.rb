require File.expand_path '../spec_helper.rb', __FILE__

describe Emogae::Site do
  let(:code) { '😎' }

  context "GET /" do
    it "should allow accessing the home page" do
      get '/'
      expect(last_response).to be_ok
    end

    context "when including code param" do
      it "redirects" do
        get '/', code: code
        expect(last_response.status).to eq 302
      end
    end
  end

  context "GET /:emoji/:title" do
    let(:title) { 'my title' }
    let(:url) { URI::encode("/#{code}/#{title}") }

    it "adds the <title> tag" do
      get url
      expect(last_response.body).to include("<title>#{title}</title>")
    end

    context "with no title" do
      let(:title) { '' }

      it "doesn't default the title" do
        get url
        expect(last_response.body).to match(/<title><\/title>/)
      end
    end

    context "with html entities in the title" do
      let(:title) { 'unescaped<title<&' }

      it "escapes title" do
        get url
        expect(last_response.body).to_not include(title)
      end
    end
  end

  context "GET /h/:human/:title" do
    let(:human) { 'baseball' }
    let(:title) { '' }
    let(:url) { "/h/#{human}/#{title}" }

    it "is successful" do
      get url
      expect(last_response.status).to eq 200
    end

    context "with invalid human string" do
      let(:human) { 'kajshd' }

      it "renders an error" do
        get url
        expect(last_response.status).to eq 404
      end
    end


  end
end
