#!/usr/bin/env ruby
# encoding: UTF-8

module Emogae
  class Site < ::Sinatra::Application
    set :site_name, "emoj.es"
    set :root, File.dirname(__FILE__) # You must set app root

    before '/' do
      str = Emogae::String.new(params[:code]) if params[:code]
      str = Emogae::String.from_param(params[:c]) if params[:c]
      str = Emogae::String.from_human(params[:human]) if params[:human]
      redirect str.to_path(params) if str
    end

    not_found do
      @error = Emogae::Errormoji.not_found
      erb :error
    end

    error do
      erb :error
    end

    helpers do
      def meta_image_url
        URI.join(request.base_url, meta_image_path)
      end

      def meta_image_path
        if error?
          error!.emoji.public_path
        else
          emoji&.public_path || '/images/emojione/1f433.png'
        end
      end

      def img(path, options={})
        include_root = options.delete(:root)
        opts = options.map{|key, value| " #{key}=\"#{value}\""}.join
        "<img src=\"#{include_root ? '/images/' : ''}#{path}\" " + opts + " />"
      end

      def css(name, options={})
        opts = options.map{|key, value| " #{key}=\"#{value}\""}.join
        name = name.to_s.gsub(/\.css$/, '') + '.css'
        "<link rel=\"stylesheet\" href=\"/css/#{name}\"#{opts} />"
      end

      def h(text)
        Rack::Utils.escape_html(text)
      end

      def title
        if error?
          @error.message
        elsif @string
          params[:title]
        else
          "#{settings.site_name} - embiggen your emoji"
        end
      end

      def encoded_url
        #do not use title helper, because that encodes the string and defaults
        URI.join("https://emoj.es", @string&.to_path(title: title) || '')
      end

      def emoji
        @emoji
      end

      def error?
        !@error.nil?
      end

      def error!
        @error || Emogae::Errormoji.unknown
      end

      def string
        @string
      end

      def render_emoji
        @emoji = string.emoji!
        if emoji.valid?
          erb :emoji
        else
          not_found
        end
      end
    end

    get '/' do
      erb :index
    end

    get '/about' do
      erb :about
    end

    get '/random/?:count?' do
      count = [3, params[:count] || 0].max
      input = Dir["#{Emogae::Emoji.local_path}/*"].sample(count)
        .map{|file| file.match(/\/([\w-]+)\.png/)[1] }
        .map{|match| match.split('-')}
        .map{|code_arrays| code_arrays.map(&:hex).pack('U')}
        .join

      @string = Emogae::String.new(input)
      render_emoji
    end

    get '/c/:string/?:title?' do
      @string = Emogae::String.from_param(params[:string])
      render_emoji
    end

    get '/h/:human/?:title?' do
      @string = Emogae::String.from_human(params[:human])
      render_emoji
    end

    get '/:emojis/?:title?' do
      @string = Emogae::String.new(params[:emojis])
      render_emoji
    end
  end
end
