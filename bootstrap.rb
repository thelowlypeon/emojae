ENV['GEM_PATH'] = 'vendor/'
require 'bundler/setup'

ENV['ENVIRONMENT'] ||= 'development'
Bundler.require :default, ENV['ENVIRONMENT'].to_sym

Dir["./lib/*.rb"].each {|file| require file }
require './emogae.rb'
