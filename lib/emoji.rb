module Emogae
  class Emoji
    attr_reader :codes, :filename

    def self.poop
      self.new(['1f4a9'])
    end

    def self.blush
      self.new(['1f633'])
    end

    def initialize(unicode_chars, options={})
      @codes = Array(unicode_chars)
      @filename ||= "#{code}.png"
      @compilation = options[:compilation] == true
    end

    def code
      codes.join('-')
    end

    def valid?
      !codes.compact.empty? && file_exists?
    end

    def valid_for_build?
      !codes.empty? && !file_exists?
    end

    def file_exists?
      File.exist?(local_path)
    end

    def compilation?
      @compilation
    end

    def build(emojis)
      @compilation = true
      Emogae::ImageHelper.shared.join_emojis(emojis, local_path)
    end

    def local_path
      File.join(self.class.local_path(compilation?), filename)
    end

    def public_path
      File.join(self.class.public_path(compilation?), filename) if valid? && file_exists?
    end

    def name
      self.class.names[code]
    end

    def self.public_path(compilation = false)
      if compilation
        File.join('', 'images', 'cache')
      else
        File.join('', 'images', 'emojione')
      end
    end

    def self.local_path(compilation = false)
      if compilation
        File.join(Emogae::Site.settings.root, 'public', 'images', 'cache')
      else
        File.join(Emogae::Site.settings.root, 'public', 'images', 'emojione')
      end
    end

    def self.json
      JSON.parse(File.read(File.join(Emogae::Site.settings.root, 'public', 'scripts', 'emoji.json')))
    end

    # { shortname => unicode }
    def self.data
      @@data ||= begin
        h = {}
        json.each do |unicode, details|
          ([details['shortname'] || ''] + (details['shortname_alternates'] || [])).each do |shortname|
            human = shortname_to_human(shortname)
            h[human] = unicode unless h.key?(human)
            h[shortname] = unicode unless h.key?(shortname)
          end
        end
        h
      end
    end

    # { unicode => name }
    def self.names
      @@names ||= json.map { |unicode, details| [unicode, shortname_to_human(details['shortname'])] }.to_h
    end

    def self.shortname_to_human(shortname)
      shortname.gsub(':', '')
    end
  end
end
