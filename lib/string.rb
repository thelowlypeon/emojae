module Emogae
  class String < ::String
    VARIATION_SELECTOR = 'feof'.freeze
    ZERO_WIDTH_JOINER = '200d'.freeze

    def self.from_param(param)
      Emogae::String.new(param.split('-').map{|c| c.to_i(16)}.pack('U*'))
    end

    def self.from_human(str)
      str = str.split(',')
        .map(&:strip)
        .map { |word|
          Emogae::Emoji.data[word]
        }.compact
        .join('-')
      from_param(str)
    end

    def to_param
      unicodes.join('-')
    end

    def escape(str)
      URI.escape(str, Regexp.new("[^-_A-Za-z0-9]", false, 'N'))
    end

    def to_path(args)
      [
        'c',
        self.to_param,
        args[:title] ? escape(args[:title]) : nil
      ].compact.join('/')
    end

    def unicodes
      @unicodes ||= self.codepoints
        .map{|point| point.to_s(16) }
        .reject { |s| [VARIATION_SELECTOR, ZERO_WIDTH_JOINER].include?(s) }
    end

    def emojis
      @emojis ||= find_emojis
    end

    def emoji
      emojis.count == 1 ? emojis.first : compilation
    end

    def emoji!
      if emoji.compilation? && !emoji.file_exists?
        build_compilation
      end
      emoji
    end

    def compilation
      @compilation ||= Emogae::Emoji.new(unicodes, compilation: true)
    end

    def compilation_exists?
      emoji.file_exists?
    end

    def build_compilation
      compilation.build(emojis) if emoji.valid_for_build?
    end

    def humanize
      emojis.map(&:name).join(', ').gsub(/_/, ' ')
    end

    private

    def find_emojis
      emojis = []
      position = 0
      while position < unicodes.count do
        emoji = nil
        [3,2,1].each do |num_chars|
          next if num_chars > unicodes.count
          emoji = Emoji.new(unicodes[position, num_chars])
          if emoji.valid?
            position += num_chars
            emojis << emoji
            break
          end
        end
        position += 1 unless emoji.valid?
      end
      emojis.compact
    end
  end
end

