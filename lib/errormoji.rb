module Emogae
  class Errormoji < StandardError
    attr_accessor :emoji

    class << self
      def unknown
        new('Something bad happened!')
          .tap{|e| e.emoji = Emogae::Emoji.blush }
      end

      def not_found
        new("That emoji couldn't be found!")
          .tap{|e| e.emoji = Emogae::Emoji.poop }
      end
    end
  end
end
