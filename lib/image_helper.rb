require 'securerandom'

module Emogae
  class ImageHelper
    WIDTH = 512
    HEIGHT = 512

    def self.shared
      @@shared ||= Emogae::ImageHelper.new
    end

    # first round: put all images them horizontally
    def join_emojis(emojis, destination=nil)
      # skip if filenames are empty
      return nil if emojis.nil? || emojis.empty?

      # skip if the file already exists
      return emojis.first if emojis.count == 1

      # else build it
      image = Magick::Image.new(emojis.count * WIDTH, HEIGHT) {
        self.background_color = 'none'
        self.format = 'png'
      }
      emojis.each_with_index do |emoji, index|
        image.composite!(Magick::Image.read(emoji.local_path).first, index * WIDTH, 0, Magick::OverCompositeOp)
      end

      destination ||= File.join(Emogae::Site.settings.root, 'public', 'images', 'cache', SecureRandom.uuid)
      destination += '.png' unless destination =~ /\.png$/
      File.open(destination, 'wb') do |f|
        f.write image.to_blob
      end
      destination
    end
  end
end
